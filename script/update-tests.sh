#!/usr/bin/env sh

# Update the list of test files in meson.build

base_dir="$(realpath "$(dirname "$0")/..")"

# Output the list of test files
function test_files() {
    (cd $base_dir && find src/ -name "*.cpp" -not -name "main.cpp")
}

regexp='(test *=.*(\n.*)*sources *: *\[)(.*\n)*.*\]'
replace="\\1\n$(test_files | sed "s/.*/            '&',/")\\n            ]"
perl -i -p0e "s#$regexp#$replace#" meson.build
