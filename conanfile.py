from conans import ConanFile, Meson

class RollOrDie(ConanFile):
    name = "RollOrDie"
    version = "0.1"
    settings = "os", "compiler", "build_type", "arch"
    generators = "pkg_config"
    requires = (
            "CTRE/[>v2.4.0]@ctre/stable",
            "Catch2/[>=2.5.0]@catchorg/stable",
            "fmt/[>=5.2.1]@bincrafters/stable",
            "range-v3/[>=0.4.0]@ericniebler/stable",
            "gsl_microsoft/[>=20180102]@bincrafters/stable",
    )

    def build(self):
        meson = Meson(self)
        if self.should_configure:
            meson.configure(args=["-Db_coverage=true"])
        if self.should_build:
            meson.build()
        if self.should_test:
            meson.test(args=["-v"])

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["RollOrDie"]

