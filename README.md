# RollOrDie

(Not usable yet)

The goal is to be able to simulate dice rolling with a nice syntax. It's a
re-write of [DiceParser](https://github.com/Rolisteam/DiceParser) in C++20.

Currently, only the basic infrastructure and tooling is set-up.

# Installation

First install:
 - make
 - gcc (currently only gcc9 is supported)
 - meson
 - conan
 - cmake (to build dependencies)

Optional dependencies:
 - gcov or lcov + genhtml for code coverage
 - doxygen to generate documentation

Then simply run

    make

All dependencies will be install with conan, and meson will take care of
building everything then running the unit tests

# Tooling

The following make targets are available:

 - `make format`: apply clang-format on all source files
 - `make lint args="$file"`: run clang-tidy on $file (currently not supported
   until clang catch up on C++20)
 - `make coverage`: run coverage analysis (gcov or lcov + genhtml must be
   installed)
 - `make update-files`: update the list of files of each target in meson.build
 - `make documentation`: generate documentation with doxygen

As well as:
 - `make clean`: remove the build directory
 - `make rebuild`: update the list of files in meson.build, force re-run of
   `meson configure` then build

# License

The code is distributed under GPL-V3+. A copy of the GPL-V3 is available in the
file LICENSE.
