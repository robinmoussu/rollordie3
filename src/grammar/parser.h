#pragma once

#include <utility/string.h>
#include <utility/string_view.h>

#include <ctre.hpp>

#include <exception>
#include <iterator>
#include <string_view>
#include <tuple>
#include <type_traits>

namespace grammar {

struct parse_error : std::exception {
  const char* what() const noexcept override { return "Invalid input"; }
};

/**
 * Returns the value computed by T::parse(input)
 *
 * \note This function is intended to be used in user code instead of
 * T::parse(input) since the output is directly usable
 */
template<class T>
constexpr auto
parse(std::string_view input) {
  return T::parse(input).value;
}

/**
 * All parse() member functions will return a pair<value, std::string_view>
 */
template<class T>
struct return_value {
  T value;
  std::string_view remaining_input;
};
template<class T>
return_value(T, std::string_view)->return_value<T>;

/**
 * \param input_parser Consume characters from `input`
 * \param value_maker Consume the output of `input_parser`
 *
 * \return An instance of return_value. The value is the output of
 * `value_maker()`, and the remaining_input is `input` minus the character
 * consumed by `input_parser()`
 *
 * \throw `parse_error` if `input_parser()` wasn't able to consume at least one
 * character from the input
 */
constexpr auto
make_return_value(std::string_view input, auto input_parser, auto value_maker) {
  const auto found = input_parser(input);
  // [[ assert: std::size(found) <= std::size(input) ]]
  if (std::empty(std::string_view{ found })) {
    throw parse_error{};
  }
  auto&& remaining_input = utility::make_string_view(
    std::begin(input) + std::size(found), std::end(input));
  return return_value{ value_maker(std::move(found)), remaining_input };
}

/**
 * Consume a user defined regexp from the input
 */
template<ctll::basic_fixed_string regexp>
struct literal {
  static constexpr auto _regexp = regexp; // workaround for GCC 9 bug 88092

  static constexpr return_value<std::string_view> parse(
    std::string_view input) {
    return make_return_value(
      input,
      [](auto input) {
        return std::string_view{ ctre::search<_regexp>(input) };
      },
      [](auto found) { return found; });
  }
};

/**
 * Consume a single digit from the input an convert it as an integer (base 10)
 */
struct digit {
  static constexpr return_value<int> parse(std::string_view input) {
    return make_return_value(
      input,
      [](auto input) { return std::string_view{ ctre::search<"\\d">(input) }; },
      [](auto found) { return utility::stoi(found); });
  }
};

/**
 * Consume any number of digit from the input an convert them to an integer
 * (using base 10)
 */
struct number {
  static constexpr return_value<int> parse(std::string_view input) {
    return make_return_value(
      input,
      [](auto input) {
        return std::string_view{ ctre::search<"\\d+">(input) };
      },
      [](auto found) { return utility::stoi(found); });
  }
};

/**
 * Call Ts::parse(input) for each type in Ts... successively
 */
template<class... Ts>
struct seq {
  using base = seq<Ts...>;
  static constexpr auto parse(std::string_view input) {
    // take a pointer to not have to instanciate the type
    auto apply = [&input](auto type) {
      auto [value, new_range] = type.parse(input);
      input = new_range; // advance the input
      return value;
    };
    auto values =
      std::tuple<decltype(apply(std::declval<Ts>()))...>{ apply(Ts{})... };
    return return_value{ values, input };
  }
};

} // namespace grammar
