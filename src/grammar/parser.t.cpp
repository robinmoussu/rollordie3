#include <catch2/catch.hpp>
#include <grammar/parser.h>

#include <string>
#include <string_view>

using namespace std::literals;
using namespace grammar;

struct my_literal : literal<"aa"> {
  static constexpr return_value<std::string_view> parse(
    std::string_view input) {
    auto [value, remaining_input] = literal::parse(input);
    return { "my_literal", remaining_input };
  }
};

struct my_second_literal : my_literal {
  static constexpr return_value<std::string_view> parse(
    std::string_view input) {
    auto [value, remaining_input] = literal::parse(input);
    return { "my_second_literal", remaining_input };
  }
};

SCENARIO("literal", "[parse]") {
  // It must match simple pattern
  CHECK(parse<literal<"1">>("1") == "1");
  CHECK(parse<literal<"a">>("a") == "a");
  CHECK(parse<literal<"aa">>("aa") == "aa");
  CHECK(parse<literal<" ">>(" ") == " ");

  // It must match a string longer than the pattern
  CHECK(parse<literal<"str">>("str  ") == "str");

  // It must match the beginning of the pattern
  CHECK(parse<literal<"str">>("  str  ") != "");

  // It must match pcre regexp
  CHECK(parse<literal<"[1-9]">>("345") == "3");
  CHECK(parse<literal<"[1-9]+">>("345") == "345");
  CHECK(parse<literal<"[1-9]+">>("345__") == "345");

  // It must fail on an invalid pattern
  REQUIRE_THROWS_AS(parse<literal<"[1-9]">>("abc"), grammar::parse_error);

  // It must be able to use a user defined parse() function
  CHECK(parse<my_literal>("aa") == "my_literal");
  CHECK(parse<my_second_literal>("aa") == "my_second_literal");
  // It must also fail on invalid pattern on a user defined parse() function
  REQUIRE_THROWS_AS(parse<my_literal>("xx"), grammar::parse_error);
}

SCENARIO("digit", "[parse]") {
  // It must parse a single digit
  CHECK(parse<digit>("1") == 1);
  CHECK(parse<digit>("123abc") == 1);

  // It must fail on an invalid pattern
  REQUIRE_THROWS_AS(parse<digit>("abc"sv), grammar::parse_error);
}

SCENARIO("number", "[parse]") {
  // It must parse a number
  CHECK(parse<number>("123") == 123);
  CHECK(parse<number>("123abc") == 123);

  // It must fail on an invalid pattern
  REQUIRE_THROWS_AS(parse<number>("abc"sv), grammar::parse_error);
}

SCENARIO("seq", "[parse]") {
  // It must parse a sequene of expression
  CHECK(parse<seq<digit, digit, digit, digit>>("1234") ==
        std::tuple{ 1, 2, 3, 4 });
  CHECK(parse<seq<digit, digit>>("12__") == std::tuple{ 1, 2 });
  CHECK(parse<seq<digit, digit, literal<"!">>>("12!") ==
        std::tuple{ 1, 2, "!" });

  // It must fail on invalid pattern
  using two_digits = seq<digit, digit>;
  REQUIRE_THROWS_AS(parse<two_digits>("abc"sv), grammar::parse_error);
  REQUIRE_THROWS_AS(parse<two_digits>("1bc"sv), grammar::parse_error);

  using default_add =
    seq<number, literal<ctll::basic_fixed_string{ "\\+" }>, number>;
  struct user_provided_add : default_add {
    static constexpr auto parse(std::string_view r) {
      auto [value, new_range] = base::parse(r);
      auto [left, op, right] = value;
      if (op == "+") {
        return return_value{ left + right, new_range };
      }

      // throw not yet supported in constexpr function
      // else {
      //   throw "Operator not implemented";
      //}
      [[unreachable]];
      return return_value{ left, r };
    }
  };

  // It must uses the default parser
  CHECK(parse<default_add>("3+2") == std::tuple{ 3, "+", 2 });
  CHECK(parse<default_add>("19+23") == std::tuple{ 19, "+", 23 });

  // It must uses the parser provided provided by the user
  CHECK(parse<user_provided_add>("3+2") == 5);
  CHECK(parse<user_provided_add>("19+23") == 42);
  CHECK(parse<seq<number, my_literal, my_second_literal, number>>("1aaaa33") ==
        std::tuple{ 1, "my_literal", "my_second_literal", 33 });
}
