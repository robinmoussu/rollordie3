#pragma once

#include <numeric>
#include <string_view>

namespace utility {

/// Creates a string view from a pair of iterators
template<typename _It>
constexpr std::string_view
make_string_view(_It begin, _It end) {
  const auto size = std::distance(begin, end);
  if (size == 0) {
    // don't dereference begin in case begin is past the last element of the
    // string
    return std::string_view(nullptr, 0);
  }
  if (*(begin + size - 1) == '\0') {
    return std::string_view(&*begin, size - 1);
  }
  return std::string_view(&*begin, size);
}

} // namespace utility
