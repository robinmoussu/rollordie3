#include <utility/string_view.h>

#include <cctype>

namespace utility {

/// Convert a char representing a digit in base 10 to it's
constexpr int
ctoi(char c) {
  return c - '0';
}

/// Convert a string_view containing only valid base 10 digits
constexpr int
stoi(std::string_view sv)
// [[expects:0 < std::size(sv)]]
// [[expects:(sv[0] == '-') || std::isdigit(sv[0])]]
// [[expects:allof(std::begin(sv) + 1, std::end(sv), std::isdigit) ]]
{
  auto negative = sv[0] == '-';

  auto begin = negative ? std::begin(sv) + 1 : std::begin(sv);

  // I can't uses std::accumulete since std::accumulete isn't constexpr
  int sum = 0;
  for (auto&& c : make_string_view(begin, std::end(sv))) {
    sum = sum * 10 + ctoi(c);
  }

  if (negative) {
    return -sum;
  } else {
    return sum;
  }
}

} // namespace utility
