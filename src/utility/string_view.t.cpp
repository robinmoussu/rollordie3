#include <catch2/catch.hpp>

#include <string>
#include <string_view>

#include <utility/string_view.h>

using namespace std::literals;

SCENARIO("make_string_view", "[string_view]") {
  // bacic test
  const auto sv = "abc"sv;
  CHECK(utility::make_string_view(std::begin(sv), std::end(sv)) == "abc"sv);

  const char cstr[4] = "abc";
  CHECK(utility::make_string_view(std::begin(cstr), std::end(cstr) - 1) ==
        "abc"sv);

  const auto str = "abc"s;
  CHECK(utility::make_string_view(std::begin(str), std::end(str)) == "abc"sv);

  // substring
  const auto sv_ = "abc___"sv;
  CHECK(utility::make_string_view(std::begin(sv_), std::begin(sv_) + 3) ==
        "abc"sv);

  const char cstr_[7] = "abc___";
  CHECK(utility::make_string_view(std::begin(cstr_), std::begin(cstr_) + 3) ==
        "abc"sv);

  const auto str_ = "abc___"s;
  CHECK(utility::make_string_view(std::begin(str_), std::begin(str_) + 3) ==
        "abc"sv);

  // empty string
  const auto empty_sv = ""sv;
  CHECK(utility::make_string_view(std::begin(empty_sv), std::end(empty_sv)) ==
        ""sv);

  const char empty_cstr[1] = "";
  CHECK(utility::make_string_view(std::begin(empty_cstr),
                                  std::end(empty_cstr) - 1) == ""sv);

  const auto empty_str = ""s;
  CHECK(utility::make_string_view(std::begin(empty_str), std::end(empty_str)) ==
        ""sv);

  // string ending with a '\0'
  char null_char[] = { 'a', 'b', 'c', '\0' };
  CHECK(utility::make_string_view(std::begin(null_char), std::end(null_char)) ==
        "abc"sv);
}
