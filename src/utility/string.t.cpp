#include <catch2/catch.hpp>

#include <utility/string.h>

using namespace std::literals;

SCENARIO("ctoi", "[string]") {
  // it must convert a char (in range ['0' : '9'] to an integer representation
  CHECK(utility::ctoi('0') == 0);
  CHECK(utility::ctoi('1') == 1);
  CHECK(utility::ctoi('2') == 2);
  CHECK(utility::ctoi('3') == 3);
  CHECK(utility::ctoi('4') == 4);
  CHECK(utility::ctoi('5') == 5);
  CHECK(utility::ctoi('6') == 6);
  CHECK(utility::ctoi('7') == 7);
  CHECK(utility::ctoi('8') == 8);
  CHECK(utility::ctoi('9') == 9);
}

SCENARIO("stoi", "[string]") {
  // it must parse an int from a valid number in base 10
  CHECK(utility::stoi("0"sv) == 0);
  CHECK(utility::stoi("1"sv) == 1);
  CHECK(utility::stoi("123"sv) == 123);

  // it must parse negative numbers
  CHECK(utility::stoi("-1"sv) == -1);
  CHECK(utility::stoi("-123"sv) == -123);
}
