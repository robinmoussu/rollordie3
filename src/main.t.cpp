// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN

// main file for catch2, do not edit
#include "catch2/catch.hpp"
