all: build

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
root_dir    := $(dir $(mkfile_path))
build_dir   := $(root_dir)build
src_dir     := $(root_dir)src
script_dir  := $(root_dir)script
doc_dir     := $(root_dir)documentation

pink        := $(shell tty -s && tput bold && tput setaf 5)
bold        := $(shell tty -s && tput bold)
normal      := $(shell tty -s && tput sgr0)

################################################################################
# build commands

build: $(build_dir)/.dependencies_installed format
	@conan build --build --test -bf '$(build_dir)' '$(root_dir)'

.PHONY: clean
clean:
	@rm '$(doc_dir)' '$(build_dir)' -rf

rebuild:| force_rebuild build

################################################################################
# tooling commands

# Run clang-format on all sources
.PHONY: format
format:
	@find '$(src_dir)/' \( -name '*.cpp' -or -name '*.h' \) -exec clang-format -style=file -i {} +

# Run clang tidy on all files passed throught the `args` variable
# example: make lint args='src/main.cpp'
.PHONY: lint
lint:
	@clang-tidy -p '$(build_dir)' $(args) | sed 's:^../:$(root_dir)/:'

# run code codevrage on all the unit tests
.PHONY: coverage
coverage: build
	@(cd '$(build_dir)' && meson --internal coverage --html '$(src_dir)' '$(build_dir)' '$(build_dir)/meson-logs')

# Update the files that meson will compile
.PHONY: update-files
update-files:
	@'$(script_dir)/update-tests.sh'

# Generate documentation with doxygen
.Phony: documentation
documentation:
	@doxygen '$(root_dir)/Doxyfile'

################################################################################
# internal rules

$(build_dir)/.dependencies_installed: $(root_dir)/conanfile.py $(root_dir)/meson.build
	@conan install -if '$(build_dir)' '$(root_dir)'
	@conan build --configure -bf '$(build_dir)' '$(root_dir)'
	@touch '$(build_dir)/.dependencies_installed'

.PHONY: rebuild
force_rebuild: update-files
	@rm -f '$(build_dir)/.dependencies_installed'
